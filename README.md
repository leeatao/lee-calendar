# lee-calendar

#### 介绍
月历视图插件，依赖于Jquery的一款月历视图插件。
演示地址：http://leeatao.gitee.io/lee-calendar/
#### 软件架构
JS插件


#### 安装教程

1. 引入js文件

   ````html
   <script src="./lee-calendar.js"></script>
   ````

   

2. 引入css文件

   ````html
   <link href="./lee-calendar.css" rel="stylesheet">
   ````

   

3. 页面调用

   ````javascript
   $(".test-box").Calendar({
               calendarData: scheduleData
           });
   ````

#### 使用说明

````javascript
 * 1.使用示例：
     $(".xxx").Calendar({
            calendarData: scheduleData
        });
 *2.calendarData数据格式示例：
     var data = [
     {
         "scheduleDate":"2021-12-23",
         "domList":[{
              dom,dom,dom,dom...
          },...]
      },...];
````
